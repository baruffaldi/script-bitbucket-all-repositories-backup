# -*- coding: utf-8 -*-
#
import bitbucket
import os
import subprocess
from dulwich.repo import Repo
#from dulwich.client import HttpAuthGitClient

# RIMUOVI VECCHI REPO DA CANCELLARE
# CREA LE DIRECTORY CONTENENTI LE VARIE BRANCHES
# FETCH DOWNLOADS
# SCRIPT PER AUTO PUSH???

REP_PATH = 'Z:\\_Repositories\\bitbucket.org\\'
BARE = False

def getstatusoutput(cmd, input=False):
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	while(True):
		retcode = p.poll() #returns None while subprocess is running
		line = p.stdout.readline()
		print "X:"+line
		if line.startswith("Password"):
			print "pass requested"
			p.communicate(input)
			while p.wait() is None:
				pass
		yield line
		if(retcode is not None):
			break
	return
	
def create_repo_dir(repo, user, bb_password):
	repo_path = os.path.abspath(os.path.join(REP_PATH, "%s.%s"%(repo["owner"],repo["slug"])))
	print """
      <TreeViewNode xsi:type="BookmarkNode">
        <IsExpanded>false</IsExpanded>
        <IsLeaf>true</IsLeaf>
        <Name>%s: %s</Name>
        <Children />
        <Path>%s</Path>
        <RepoType>Git</RepoType>
      </TreeViewNode>""" % (repo["owner"],repo["slug"], repo_path)

def update_repo_dir(repo, user, bb_password):
	repo_path = os.path.abspath(os.path.join(REP_PATH, "%s.%s"%(repo["owner"],repo["slug"])))
	try:
		getstatusoutput("cd \"%s\" && git pull", bb_password)
	except:
		pass


try:
	bb_config_file = "../conf.txt" # Containing user on first line and password on second line
	bb_config = open(bb_config_file).read().split('\n')
	bb_username = bb_config[0].strip()
	bb_password = bb_config[1].strip()
	bb_users_gotta_be_saved = bb_config[2].strip().split(',')
except:
	print "# Configuration not loaded"

try:
	bb = bitbucket.BitBucket(bb_username, bb_password)
	bb_user = bb.user(bb_username)
except:
	print "# Impossible to login on bitbucket"

#try:
for user in bb_users_gotta_be_saved:
	bb_repositories = bb.user(user).repositories()
	for repo in bb_repositories:
		if 1:
			create_repo_dir(repo, user, bb_password)