# -*- coding: utf-8 -*-
#
import bitbucket
from bitbucket2.bitbucket import Bitbucket
import os
import subprocess
from dulwich.repo import Repo
#from dulwich.client import HttpAuthGitClient

# RIMUOVI VECCHI REPO DA CANCELLARE
# CREA LE DIRECTORY CONTENENTI LE VARIE BRANCHES
# FETCH DOWNLOADS
# SCRIPT PER AUTO PUSH???

REP_PATH = 'Z:\\_Repositories\\bitbucket.org\\'
BARE = False

def getstatusoutput(cmd, input=False):
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	while(True):
		retcode = p.poll() #returns None while subprocess is running
		line = p.stdout.readline()
		print "X:"+line
		if line.startswith("Password"):
			print "pass requested"
			p.communicate(input)
			while p.wait() is None:
				pass
		yield line
		if(retcode is not None):
			break
	return

def get_repo_pathname(repo):
	return os.path.abspath(os.path.join(REP_PATH, "%s.%s.%s"%(repo["owner"],"private" if repo["is_private"] else "public",repo["slug"])))

def create_repo_dir(repo, user, bb_password):
	os.mkdir(get_repo_pathname(repo))
	#if BARE:
	#	os.mkdir(repo_path)
	#	local = Repo.init_bare(repo_path)
	#else:
	#	local = Repo.init(repo_path, mkdir=True)

	#client = HttpAuthGitClient('https://%s:%s@bitbucket.org/%s/%s.git'%(user, bb_password,user,repo["slug"]))
	#remote_refs = client.fetch("master", local)
	#local["HEAD"] = remote_refs["refs/heads/master"]

	#client, path = get_transport_and_path('https://%s:%s@bitbucket.org/%s/'%(user, bb_password,user))
	#remote_refs = client.fetch("%s.git" % (repo["slug"]), local)
	#local["HEAD"] = remote_refs["refs/heads/master"]
	#os.mkdir(repo_path)
	#print "mkdir \"%s\" && cd \"%s\" && git init && git remote add origin https://baruffaldi@bitbucket.org/%s/%s.git && git pull" % (repo_path,repo_path,user,repo["slug"])
	#p = subprocess.Popen("mkdir \"%s\" && cd \"%s\" && git"%(repo_path,repo_path), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	#p.communicate("git.cmd init")
	#p.communicate("remote add origin https://baruffaldi@bitbucket.org/%s/%s.git"%(user,repo["slug"]))
	#p.communicate("pull")
	#p.communicate(bb_password)
	#getstatusoutput("mkdir \"%s\""%repo_path)
	#getstatusoutput("cd \"%s\" && git.cmd init"%(repo_path))
	#getstatusoutput("cd \"%s\" && git.cmd remote add origin https://baruffaldi@bitbucket.org/%s/%s.git"%(repo_path,user,repo["slug"]))
	#getstatusoutput("cd \"%s\" && git.cmd pull" % (repo_path), bb_password)
	#print getstatusoutput("cd \"%s\" && git remote add origin git@bitbucket.org:%s/%s.git" %(repo_path,user,repo["slug"]))[1]
	#print "cd \"%s\" && git remote add origin git@bitbucket.org:%s/%s.git" %(repo["name"],user,repo["slug"])
	
def create_repo_branches_dirs(repo, user, bb_password):
	repo_path = get_repo_pathname(repo)
	branches = Bitbucket(bb_username, bb_password, repo["slug"]).get_branches()[1]
	if branches == "Service not found.":
		os.mkdir(os.path.join(repo_path,"master"))
		print "+ Branch created: master"
	else:
		for branche in branches:
			print "+ Branch created: %s" % branche
			os.mkdir(os.path.join(repo_path,branche))
	

def update_repo_dir(repo, user, bb_password):
	repo_path = get_repo_pathname(repo)
	try:
		getstatusoutput("cd \"%s\" && git pull", bb_password)
	except:
		pass

print "Bitbucket repository backup 0.1"
print "Coded by Filippo Baruffaldi"
print "-"

try:
	bb_config_file = "../conf.txt" # Containing user on first line and password on second line
	bb_config = open(bb_config_file).read().split('\n')
	bb_username = bb_config[0].strip()
	bb_password = bb_config[1].strip()
	bb_users_gotta_be_saved = bb_config[2].strip().split(',')
	print "* Configuration Loaded"
	print "*** User %s" % bb_username
	#print "*** Password %s" % bb_pass
except:
	print "# Configuration not loaded"

try:
	bb = bitbucket.BitBucket(bb_username, bb_password)
	bb_user = bb.user(bb_username)
	print "* Logged to Bitbucket"
except:
	print "# Impossible to login on bitbucket"

#try:
for user in bb_users_gotta_be_saved:
	bb_repositories = bb.user(user).repositories()
	print "-"
	print "* Repositories fetched for user: %s" % user
	print "-"
	for repo in bb_repositories:
		if not os.path.exists(get_repo_pathname(repo)):
			create_repo_dir(repo, user, bb_password)
			print "+ Repository created: %s" % repo["name"]
			create_repo_branches_dirs(repo, user, bb_password)
			#break
		#else:
		#	print "+ Repository found: %s" % repo["name"]
		#	update_repo_dir(repo, user, bb_password)
#except:
#	print "# Impossible to retrieve repositories"
#cmd = raw_input('Press any key to close me')
