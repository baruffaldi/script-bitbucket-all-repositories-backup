# Set-ExecutionPolicy unrestricted

# initialize the items variable with the
# contents of a directory

$repos_basepath = "Z:\_Repositories\bitbucket.org\"

Function Update($name, $path)
{
	Write-Host �Collecting branches... $path� 
	$items = Get-ChildItem -Path $path
	# enumerate the items array
	foreach ($item in $items)
	{
		  #Write-Host Branche: $item� 
		  # if the item is a directory, then process it.
		  if ($item.Attributes -eq "Directory")
		  {
				UpdateRepo $item $name �$path\$item�
		  }
	}
}

Function UpdateRepo($branchename, $name, $path)
{
	Write-Host �Updating branche... $branchename� 
	$user = ""
	if ($name.Name.StartsWith("baruffaldi.")){
		if ($name.Name.StartsWith("baruffaldi.public.")){
			$type = "public"
			$rep = $name.Name.Substring(18)
		} else {
			$type = "private"
			$rep = $name.Name.Substring(19)
		}
		Write-Host �Updating ($type) $rep of user: baruffaldi�
		cd $path
		git init
		git remote add origin ssh://git@bitbucket.org/baruffaldi/$rep.git
	} elseif ($name.Name.StartsWith("gbc.")){
		if ($name.Name.StartsWith("gbc.public.")){
			$type = "public"
			$rep = $name.Name.Substring(11)
		} else {
			$type = "private"
			$rep = $name.Name.Substring(12)
		}
		Write-Host �Updating ($type) $rep of user: gbc�
		cd $path
		git init
		git remote add origin ssh://git@bitbucket.org/gbc/$rep.git
	}
}

Write-Host �BitBucket.org Backup All Repos�
Write-Host �Coded by Filippo Baruffaldi for GB Consulting�
Write-Host �www.baruffaldi.info - www.gbc-italy.com�
Write-Host �-�
Write-Host �Collecting items (repos)...� 
$items = Get-ChildItem -Path $repos_basepath
# enumerate the items array
foreach ($item in $items)
{
      # if the item is a directory, then process it.
      if ($item.Attributes -eq "Directory")
      {
			Write-Host �Repository: $item� 
            Update $item �$repos_basepath$item�
      }
}
